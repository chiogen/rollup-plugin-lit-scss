import { Plugin } from 'rollup'
import { createFilter } from '@rollup/pluginutils';
import { compile } from 'sass'
import type { Options } from 'sass';

export type CompilerOptions = Options<'sync'>

export interface LitScssOptions {
    include?: Array<string | RegExp> | string | RegExp | null
    exclude?: Array<string | RegExp> | string | RegExp | null
    includePaths?: string[],
    compilerOptions?: CompilerOptions
}


const defaultInclude = '**/*.scss';

export function litScss(options: LitScssOptions = {}): Plugin {

    if (!options.include) {
        options.include = [
            defaultInclude
        ];
    }

    const filter = createFilter(options.include, options.exclude);

    return {
        name: 'rollup-plugin-lit-scss',

        load(id) {
            if (!filter(id)) return null;

            const result = compile(id, options.compilerOptions);
            return wrapModule(result.css);
        }
    };
}

export default litScss;

function wrapModule(css: string) {
    return `import { css } from 'lit'; export default css\`${css}\`;`;
}