# rollup-plugin-lit-scss

Resolve scss modules in rollup.

## Install
```bash
npm install @chiogen/rollup-plugin-lit-scss
```

## Example
```javascript
// rollup.config.js
import litScss from '@chiogen/rollup-plugin-lit-scss
export default {
    plugins: [
        resolve(),
        litScss()
    ]
}
```